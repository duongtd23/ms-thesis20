### Other invariants

- `_depen`: dependencies between invariants.
- `inv50`, `inv51`,..., `inv59`: proof scores for corresponding invariant/lemma.