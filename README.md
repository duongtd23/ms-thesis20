# ms-thesis20

- `simple-nat.cafe`: simple natural number and the proof scores to prove that the plus function is associative.
- `anderson` folder: formal verification of an abstract version of Anderson protocol. 
- `mcs` folder: formal verification of MCS protocol. 